use [Mariya Hrendus]
go
create or alter view visit_check as select * from visit 
where reason = 'headache'
with check option
go
select * from visit_check
go
use [Mariya Hrendus]
go
create or alter view resident_visit
(visit_id, reason, visit_date, resident_id)
as select visit_id, reason, visit_date, resident_id
from visit
select * from resident_visit
go

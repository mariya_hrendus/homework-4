use [Mariya Hrendus]
go
create or alter view resident_information
(id, first_name, surname, lastname, birthday, blood_group, disease_information, allergy_information, phone_number, note)
as select id, first_name, surname, lastname, birthday, blood_group, disease_information, allergy_information, phone_number, note
from resident
select * from resident_information
go